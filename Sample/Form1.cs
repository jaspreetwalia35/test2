﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Looking Up Customer");

            if (textBox1.Text == "99904552" ) 
            {
                label2.Text = " Welcome Albert Danison ..!";
            }
            
            else
            {
                label2.Text = " Error : Customer Not Found ";
            }
        }
    }
}
